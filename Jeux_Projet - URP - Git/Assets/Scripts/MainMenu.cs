﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private int level;
    private int doorUnlockedNumber;
    private FadeIn_Out fadeIn_Out;

    void Start()
    {
        if(GameObject.FindGameObjectWithTag("Fading") == true)
        {
            fadeIn_Out = GameObject.FindGameObjectWithTag("Fading").GetComponent<FadeIn_Out>();
        }
    }
    public void NewGame()
    {
        fadeIn_Out.FadeTotalDeath();
        SceneManager.LoadScene("Level01");
    }

    public void ContinueGame()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        level = data.level;
        SceneManager.LoadScene(level);
        doorUnlockedNumber = data.doorUnlockedNumber;
        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        transform.position = position;
    }

    public void QuitGame()
    {
        Application.Quit();
    }


}
