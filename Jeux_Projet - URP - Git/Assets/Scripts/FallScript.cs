﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FallScript : MonoBehaviour
{
    private FadeIn_Out fadeIn_Out;
    private PlayerParameters playerParameters;
    //private Vector3 posPlayer;
    private Animator animator;
    private bool istriggered = false;

    void Start()
    {
        animator = GameObject.FindGameObjectWithTag("Fading").GetComponent<Animator>();
        //posPlayer = GameObject.FindGameObjectWithTag("Player").transform.position;
        fadeIn_Out = GameObject.FindGameObjectWithTag("Fading").GetComponent<FadeIn_Out>();
        playerParameters = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerParameters>();
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (istriggered == false)
            {
                playerParameters.LostBatteryPoints();
                if (playerParameters.HowMuchBatteryPoints() > 0)
                {
                    //fadeIn_Out.FadeToLevel(SceneManager.GetActiveScene().buildIndex);
                    animator.SetTrigger("FadeOutDeath");
                    istriggered = true;
                }
                if (playerParameters.HowMuchBatteryPoints() <= 0)
                {
                    istriggered = true;
                }
            }
        }
    }

    public void MakeTriggerFalse()
    {
        istriggered = false;
        //Debug.Log("Collider01");
    }

}
