﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadeIn_Out : MonoBehaviour
{
    public Animator animator;
    private int levelToLoad;

    private PlayerParameters playerParameters;
    private GameObject[] resetFallCollider;
    private FallScript[] fallScript;
    private GlobalControl globalControl;

    private GameObject[] actionObject;

    private GameObject player;
    private Vector3 posPlayer;
    private Quaternion rotPlayer;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerParameters = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerParameters>();

        resetFallCollider = GameObject.FindGameObjectsWithTag("FallCollider");
        fallScript = new FallScript[resetFallCollider.Length];

        for (int i = 0; i < fallScript.Length; i++)
        {
            fallScript[i] = resetFallCollider[i].GetComponent<FallScript>();
        }

        globalControl = GameObject.FindGameObjectWithTag("GlobalControl").GetComponent<GlobalControl>();


        posPlayer = player.transform.position;
        rotPlayer = player.transform.rotation;

        //Debug.Log(posPlayer);
    }

    public void FadeToNextLevel()
    {
        FadeToLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void FadeToBeforeLevel()
    {
        FadeToLevel(SceneManager.GetActiveScene().buildIndex - 1);
    }
    public void FadeToLevel(int levelIndex)
    {
        //Debug.Log(levelIndex);
        levelToLoad = levelIndex;
        animator.SetTrigger("FadeOut");
    }
    public void FadeTotalDeath()
    {
            for (int i = 0; i < 6; i++)
            {
            //Debug.Log(i);
                playerParameters.TotalDeath();
                SceneManager.LoadScene(i);
                playerParameters.TotalDeath();
                //Debug.Log(SceneManager.GetActiveScene().buildIndex);
                globalControl.ResetAllLevel();
            }
            playerParameters.BatteryPointsToTen();
            SceneManager.LoadScene(0);
    }

    public void FadeDeath()
    {
        GameObject.FindGameObjectWithTag("Player").transform.position = posPlayer;
        GameObject.FindGameObjectWithTag("Player").transform.rotation = rotPlayer;
        for(int i = 0; i < fallScript.Length; i++)
        {
            fallScript[i].MakeTriggerFalse();
        }
    }
    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelToLoad);
    }
}
