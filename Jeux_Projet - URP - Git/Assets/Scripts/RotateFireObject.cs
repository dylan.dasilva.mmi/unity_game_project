﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFireObject : MonoBehaviour
{
    private Vector3 rotationDirection = new Vector3(0.0f, 0.0f, -0.1f);
    private float smoothTime = 2.5f;
    private float convertedTime = 50;
    private float smooth;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        smooth = Time.deltaTime * smoothTime * convertedTime;
        transform.Rotate(rotationDirection * smooth);
    }
}
