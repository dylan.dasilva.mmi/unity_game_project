﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalControl : MonoBehaviour
{
    private Light lt;
    private ActionScript actionScript;
    private PickObject pickObject;
    private PlayerParameters playerParameters;
    private FallingPlatformManager fallingPlatformManager;
    private GameObject uiCollider;

    public static GlobalControl Instance;

    public static int doorUnlockedNumber = 0;
    private bool doorFinalLocked;


    void Awake()
    {
        if (GameObject.FindGameObjectWithTag("GlobalControl") == true)
        {
            actionScript = GameObject.FindGameObjectWithTag("GlobalControl").GetComponent<ActionScript>();
        }
        if (GameObject.FindGameObjectWithTag("GlobalControl") == true)
        {
            pickObject = GameObject.FindGameObjectWithTag("GlobalControl").GetComponent<PickObject>();
        }
        if(GameObject.FindGameObjectWithTag("GlobalControl") == true)
        {
            fallingPlatformManager = GameObject.FindGameObjectWithTag("GlobalControl").GetComponent<FallingPlatformManager>();
        }
        if (GameObject.FindGameObjectWithTag("Player") == true)
        {
            playerParameters = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerParameters>();
        }

        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (doorFinalLocked == true)
        {
            GameObject.FindGameObjectWithTag("DoorLockedFinal").GetComponent<BoxCollider>().enabled = true;
        }

        if (SceneManager.GetActiveScene().buildIndex == 6 || SceneManager.GetActiveScene().buildIndex == 16 || SceneManager.GetActiveScene().buildIndex == 25)
        {
            actionScript.ResetAction();
            playerParameters.BatteryPointsToTen();
            doorUnlockedNumber = 0;
            //Debug.Log(doorUnlockedNumber);
        }

        if (GameObject.FindGameObjectWithTag("LightToChange") == true)
        {
            lt = GameObject.FindGameObjectWithTag("LightToChange").GetComponent<Light>();
        }
        else
        {
            lt = null;
        }

        if (doorUnlockedNumber == 0 && lt != null)
        {
            lt.color = Color.red;
            if(GameObject.FindGameObjectWithTag("DoorLocked") == true)
            {
                GameObject.FindGameObjectWithTag("DoorLocked").GetComponent<BoxCollider>().enabled = false;
                if (GameObject.FindGameObjectWithTag("ColliderUI") == true)
                {
                    GameObject.FindGameObjectWithTag("ColliderUI").GetComponent<BoxCollider>().enabled = true;
                }
            }
        }

        if (doorUnlockedNumber == 1 && lt != null)
        {
            lt.color = Color.green;
            if (GameObject.FindGameObjectWithTag("DoorLocked") == true)
            {
                GameObject.FindGameObjectWithTag("DoorLocked").GetComponent<BoxCollider>().enabled = true;
                if (GameObject.FindGameObjectWithTag("ColliderUI") == true)
                {
                    GameObject.FindGameObjectWithTag("ColliderUI").GetComponent<BoxCollider>().enabled = false;
                }
            }
        }
    }

    public void ResetAllLevel()
    {
        if (GameObject.FindGameObjectWithTag("GlobalControl") == true)
        {
            actionScript.ResetAction();
        }
        if (GameObject.FindGameObjectWithTag("GlobalControl") == true)
        {
            pickObject.ResetPickObject();
        }
        switch (doorUnlockedNumber)
        {
            case 0:
                doorUnlockedNumber = 0;
                break;
            case 1:
                doorUnlockedNumber = 0;
                break;
            default:
                Debug.Log("Bug, Unknown");
                break;
        }
    }

    public void DoorUnlocked()
    {
        doorUnlockedNumber += 1;
    }

    public void DoorUnlockedFinal()
    {
        doorFinalLocked = true;
    }
}
