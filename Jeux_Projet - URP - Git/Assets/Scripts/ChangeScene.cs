﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScene : MonoBehaviour
{
    //FadeIn_Out fadeIn_Out;
    public FadeIn_Out fadeIn_Out;
    public int sceneNumberToGo;

    public bool thereIsConditions = false;

    void Start()
    {

        fadeIn_Out = GameObject.FindGameObjectWithTag("Fading").GetComponent<FadeIn_Out>();
    }
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Test");
        //DontDestroyOnLoad(transform.gameObject);
        if(thereIsConditions == false)
        {
           fadeIn_Out.FadeToLevel(sceneNumberToGo);
        }
        if (thereIsConditions == true)
        {
            fadeIn_Out.FadeToLevel(sceneNumberToGo);
        }
    }
}
