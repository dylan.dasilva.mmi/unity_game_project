﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PickObject : MonoBehaviour
{
    private GlobalControl globalControl;

    static private bool objectIsPicked = true;
    private bool finalPile;

    void Start()
    {
        if (GameObject.FindGameObjectWithTag("GlobalControl") == true)
        {
            //Debug.Log("test");
            globalControl = GameObject.FindGameObjectWithTag("GlobalControl").GetComponent<GlobalControl>();
            //Debug.Log(globalControl);
        }

        if (GameObject.FindGameObjectWithTag("FinalPile") == true)
        {
            finalPile = true;
            objectIsPicked = false;
            /*Debug.Log(finalPile);
            Debug.Log(objectIsPicked);*/
        }

        //DontDestroyOnLoad(gameObject);
        if (gameObject.GetComponent<Renderer>() == true)
        {
            if (objectIsPicked == true)
            {
                gameObject.GetComponent<Renderer>().enabled = false;
            }
            if (objectIsPicked == false)
            {
                gameObject.GetComponent<Renderer>().enabled = true;
            }
        }
    }

    void Update()
    {
        //Debug.Log(SceneManager.GetActiveScene().buildIndex);
        if (SceneManager.GetActiveScene().buildIndex == 6 || SceneManager.GetActiveScene().buildIndex == 16 || SceneManager.GetActiveScene().buildIndex == 25)
        {
            //Debug.Log(objectIsPicked);
            objectIsPicked = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (objectIsPicked == false && finalPile == false)
        {
            globalControl.DoorUnlocked();
            gameObject.GetComponent<Renderer>().enabled = false;
            objectIsPicked = true;
        }

        if (objectIsPicked == false && finalPile == true)
        {
            globalControl.DoorUnlockedFinal();
        }
    }

    public void ResetPickObject()
    {
            objectIsPicked = false;
    }
}
