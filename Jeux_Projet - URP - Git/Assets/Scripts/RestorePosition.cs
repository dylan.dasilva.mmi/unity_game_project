﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestorePosition : MonoBehaviour
{
    private Vector3 pos;
    private Vector3[] dir;
    static private Vector3 posOriginal;

    private float[] distance;
    private int numberOfDoors;

    private GameObject player;
    private GameObject[] doors;


    void Start() // Check if we've saved a position for this scene; if so, go there. And Add and Offset to not Jump between scene in Collider.
    {
            //Debug.Log(posOriginal);
            player = GameObject.FindGameObjectWithTag("Player");
            doors = GameObject.FindGameObjectsWithTag("Doors");

            numberOfDoors = GameObject.FindGameObjectsWithTag("Doors").Length;
            distance = new float[numberOfDoors];
            dir = new Vector3[numberOfDoors];

            int sceneIndex = SceneManager.GetActiveScene().buildIndex;
            if (SavePosition.savedPositions.ContainsKey(sceneIndex))
            {
                pos = SavePosition.savedPositions[sceneIndex];
                /*for (int i = 0; i < doors.Length; i++)
                {
                    distance[i] = Vector3.Distance(pos, doors[i].transform.position);
                }*/

                for (int i = 0; i < doors.Length; i++)
                {
                    distance[i] = Vector3.Distance(pos, doors[i].transform.position);
                    if (distance[i] < 6)
                    {
                        transform.position = doors[i].transform.position;
                        transform.rotation = doors[i].transform.rotation;
                    }
                }
            }
    }

    void Update()
    {
        //Debug

        /*pos = player.transform.position;

        /*distance[0] = Vector3.Distance(pos, doors[0].transform.position);
        Debug.Log(distance[0]);
        if(distance[0] < 7)
        {
            distance[0] += 10;
        }*/
    }
        void OnDestroy() // Unloading scene, so save position.
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        SavePosition.savedPositions[sceneIndex] = transform.position;
    }
}
