﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    private Transform posResPlatform;
    private Rigidbody rb;
    public float timeBeforeFall;

    private FallingPlatformManager fallingPlatformManager;
    // Start is called before the first frame update
    void Start()
    {
        fallingPlatformManager = GameObject.FindGameObjectWithTag("GlobalControl").GetComponent<FallingPlatformManager>();
        rb = gameObject.GetComponent<Rigidbody>();
        posResPlatform = gameObject.transform;
        //Debug.Log(posResPlatform);
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Player")
        {
            /*fallingPlatformManager.RespawnPlatformTest();
            Debug.Log(gameObject);*/
            DropPlatform();
            Destroy(gameObject, timeBeforeFall);
        }
    }

    public void DropPlatform()
    {
        rb.isKinematic = false;
    }

    /*IEnumerator RespawnPlatform()
    {
        float currCountdownValue = 1f;
        while (currCountdownValue > 0)
        {
            //Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }
        Instantiate(gameObject, posResPlatform);
    }*/
}
