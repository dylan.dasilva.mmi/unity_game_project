﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneBeforeLevel : MonoBehaviour
{
    FadeIn_Out fadeIn_Out;

    void Start()
    {
        fadeIn_Out = GameObject.FindGameObjectWithTag("Fading").GetComponent<FadeIn_Out>();
    }
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Test");
        //DontDestroyOnLoad(transform.gameObject);
        fadeIn_Out.FadeToBeforeLevel();
    }
}
