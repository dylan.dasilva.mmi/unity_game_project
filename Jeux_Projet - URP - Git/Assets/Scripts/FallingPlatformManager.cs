﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatformManager : MonoBehaviour
{
    private FallingPlatform fallingPlatform;
    private GameObject[] platformFalling;
    private Transform[] posPlatformFalling;
    // Start is called before the first frame update

    void Start()
    {
        platformFalling = GameObject.FindGameObjectsWithTag("PlatformFalling");
        posPlatformFalling = new Transform[platformFalling.Length];
        for (int i = 0; i < platformFalling.Length; i++)
        {
            posPlatformFalling[i] = platformFalling[i].transform;
        }
    }
    public void RespawnPlatformTest()
    {
        StartCoroutine(RespawnPlatform());
    }

    public IEnumerator RespawnPlatform()
    {
        float currCountdownValue = 1f;
        while (currCountdownValue > 0)
        {
            //Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }
        for (int i = 0; i < platformFalling.Length; i++)
        {
            Instantiate(platformFalling[i], posPlatformFalling[i]);
        }
    }
}
