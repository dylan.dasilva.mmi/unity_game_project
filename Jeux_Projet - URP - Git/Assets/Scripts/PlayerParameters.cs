﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerParameters : MonoBehaviour
{
    static private Vector3 posOriginal;
    static private int batteryPoints = 10;
    private GameObject[] batteryImages = new GameObject[10];
    public int doorUnlockedNumber;


    private FadeIn_Out fadeIn_Out;
    private GlobalControl globalControl;
    private bool istriggered = false;

    private Animator animator;

    private GameObject[] platformNotFalling;
    private Material[] matPlatform;

    void Start()
    {
        posOriginal = transform.position;

        fadeIn_Out = GameObject.FindGameObjectWithTag("Fading").GetComponent<FadeIn_Out>();
        globalControl = GameObject.FindGameObjectWithTag("GlobalControl").GetComponent<GlobalControl>();
        animator = GameObject.FindGameObjectWithTag("Fading").GetComponent<Animator>();

        doorUnlockedNumber = GlobalControl.doorUnlockedNumber;
        //Debug.Log(doorUnlockedNumber);
        //Debug.Log(batteryPoints);
        if (GameObject.FindWithTag("Fading") == true)
        {
            for(int i = 0; i < batteryImages.Length; i++)
            {
                batteryImages[i] = GameObject.FindGameObjectWithTag("BatteryImagesArray").transform.GetChild(9 - i).gameObject;
            }
        }

        if(GameObject.FindGameObjectWithTag("PlatformNotFalling") == true)
        {
           platformNotFalling = GameObject.FindGameObjectsWithTag("PlatformNotFalling");
           for(int i = 0; i < platformNotFalling.Length; i++)
            {
                Destroy(platformNotFalling[i].GetComponent<FallingPlatform>());
            }
           matPlatform = new Material[platformNotFalling.Length];
           for (int i = 0; i < platformNotFalling.Length; i++)
            {
                //Debug.Log(platformNotFalling[i]);
                matPlatform[i] = platformNotFalling[i].GetComponent<Renderer>().material;
            }
        }
    }
    void Update()
    {
        //Debug.Log(batteryPoints);
        if (batteryPoints == 10)
        {
            for (int i = 0; i < batteryImages.Length; i++)
            {
                batteryImages[i].SetActive(true);
            }
        }
        if (batteryPoints < 10)
        {
            batteryImages[0].SetActive(false);
        }
        if (batteryPoints < 9)
        {
            batteryImages[1].SetActive(false);
        }
        if (batteryPoints < 8)
        {
            batteryImages[2].SetActive(false);
        }
        if (batteryPoints < 7)
        {
            batteryImages[3].SetActive(false);
        }
        if (batteryPoints < 6)
        {
            batteryImages[4].SetActive(false);
        }
        if (batteryPoints < 5)
        {
            batteryImages[5].SetActive(false);
        }
        if (batteryPoints < 4)
        {
            batteryImages[6].SetActive(false);
        }
        if (batteryPoints < 3)
        {
            batteryImages[7].SetActive(false);
        }
        if (batteryPoints < 2)
        {
            batteryImages[8].SetActive(false);
        }
        if (batteryPoints < 1)
        {
            batteryImages[9].SetActive(false);
        }
        if(batteryPoints <= 0)
        {
            animator.SetTrigger("FadeOutTotalDeath");
            //fadeIn_Out.FadeTotalDeath();
        }

        if(GameObject.FindGameObjectWithTag("PlatformFalling") == true & GameObject.FindGameObjectWithTag("PlatformNotFalling"))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                LostBatteryPoints();
                FindTheWay();
                /*SaveGame();
                Debug.Log(this);*/
            }
        }

    }

    public void BatteryPointsToTen()
    {
        batteryPoints = 10;
    }
    public void LostBatteryPoints()
    {
        //Debug.Log("test");
        batteryPoints -= 1;
    }
    public void AddBatteryPoints()
    {
        //Debug.Log("test");
        batteryPoints = 10;
    }

    public int HowMuchBatteryPoints()
    {
        int batteryPointsToReturn;
        batteryPointsToReturn = batteryPoints;
        return batteryPointsToReturn;
    }

    public void TotalDeath()
    {
        //Debug.Log("Test");
        gameObject.transform.position = posOriginal;
    }

    public void FindTheWay()
    {
        for (int i = 0; i < matPlatform.Length; i++)
        {
            matPlatform[i].SetColor("_BaseColor", new Vector4(0f, 0f, 0f));
        }
        StartCoroutine(FindPlatform());
    }

    void OnTriggerEnter(Collider other)
    {
        if (istriggered == false)
        {
            istriggered = true;
        }
    }

    public void SaveGame()
    {
        gameSaved();
        //Debug.Log("PartieSauvegardée");
    }

    public void gameSaved()
    {
        SaveSystem.SavePlayer(this);
    }

    IEnumerator FindPlatform()
    {
        float currCountdownValue = 1.5f;
        while (currCountdownValue > 0)
        {
            //Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }
        for (int i = 0; i < matPlatform.Length; i++)
        {
            matPlatform[i].SetColor("_BaseColor", new Vector4(1f, 1f, 1f));
        }
    }
}
