﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ActionScript : MonoBehaviour
{
    private GameObject[] platformToMove;
    private PlayerParameters playerParameters;

    static private bool plateformeActive;
    static private bool plateformeActive02;
    public int plateformeNiveau;

    public int kindOfActions;
    // Start is called before the first frame update
    void Start()
    {
        //DontDestroyOnLoad(gameObject);
        if (GameObject.FindGameObjectWithTag("ActionObject") == true)
        {
            platformToMove = GameObject.FindGameObjectsWithTag("ActionObject");
            //Debug.Log(platformToMove);
            if (plateformeActive == false)
            {
                if (platformToMove.Length > 0)
                {
                    for (int i = 0; i < platformToMove.Length; i++)
                    {
                        //Debug.Log(platformToMove[i]);
                        platformToMove[i].GetComponent<PlatformMoving>().enabled = false;
                    }
                }
            }

            /*if (plateformeActive02 == false)
            {
                if (platformToMove.Length > 0)
                {
                    for (int i = 0; i < platformToMove.Length; i++)
                    {
                        //Debug.Log(platformToMove[i]);
                        platformToMove[i].GetComponent<PlatformMoving>().enabled = false;
                    }
                }
            }*/

            if (plateformeActive == true)
            {
                for (int i = 0; i < platformToMove.Length; i++)
                {
                    platformToMove[i].GetComponent<PlatformMoving>().enabled = true;
                }
            }
            /*if (plateformeActive02 == true && SceneManager.GetActiveScene().buildIndex == 4)
            {
                for (int i = 0; i < platformToMove.Length; i++)
                {
                    platformToMove[i].GetComponent<PlatformMoving>().enabled = true;
                }
            }*/

            if (GameObject.FindGameObjectWithTag("Player") == true)
            {
                playerParameters = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerParameters>();
            }
            //plateformeActive = false;
            Debug.Log(platformToMove.Length);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectWithTag("ActionObject") == true && kindOfActions == 0)
        {
            platformToMove = GameObject.FindGameObjectsWithTag("ActionObject");
            //Debug.Log(platformToMove);
            if (plateformeActive == false)
            {
                if (platformToMove.Length > 0)
                {
                    for (int i = 0; i < platformToMove.Length; i++)
                    {
                        //Debug.Log(platformToMove[i]);
                        platformToMove[i].GetComponent<PlatformMoving>().enabled = false;
                    }
                }
            }
        }

            //Debug.Log(plateformeActive);
        if (plateformeActive == true && GameObject.FindGameObjectWithTag("ActionObject") == true && kindOfActions == 0)
        {
            for (int i = 0; i < platformToMove.Length; i++)
            {
                //Debug.Log(platformToMove[i]);
                platformToMove[i].GetComponent<PlatformMoving>().enabled = true;
            }
        }

        if (GameObject.FindGameObjectWithTag("Player") == true)
        {
            //Debug.Log(playerParameters);
            playerParameters = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerParameters>();
        }
    }

    void OnTriggerStay(Collider other)
    {
        //Debug.Log("PressE");
        if (other.gameObject.tag == "Player")
        {
            //Debug.Log("Appuyez sur E");
            if (Input.GetKey(KeyCode.E) && plateformeActive == false && kindOfActions == 0 && plateformeNiveau == 0)
            {
                Debug.Log("PlateformeActivé");
                playerParameters.LostBatteryPoints();
                plateformeActive = true;
            }
            if (Input.GetKey(KeyCode.E) && plateformeActive02 == false && kindOfActions == 0 && plateformeNiveau == 1)
            {
                Debug.Log("PlateformeActivé");
                for (int i = 0; i < platformToMove.Length; i++)
                {
                    //Debug.Log(platformToMove[i]);
                    platformToMove[i].GetComponent<PlatformMoving>().enabled = true;
                }
                playerParameters.LostBatteryPoints();
                plateformeActive02 = true;
            }

            if (Input.GetKey(KeyCode.E) && kindOfActions == 1)
            {
                Debug.Log("BatteryRechargé");
                playerParameters.AddBatteryPoints();
            }
            /*if (Input.GetKey(KeyCode.R))
            {
                Debug.Log("BatteryRechargé");
                playerParameters.AddBatteryPoints();
            }*/
        }
    }

        public void ResetAction()
        {
        plateformeActive = false;
        plateformeActive02 = false;
        }
}
