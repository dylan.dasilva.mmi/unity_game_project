﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveButton : MonoBehaviour
{
    private PlayerParameters playerParameters;
    private ShowUI showUI;
    void Start()
    {
        playerParameters = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerParameters>();
        showUI = GameObject.FindGameObjectWithTag("ColliderUI").GetComponent<ShowUI>();
    }

    void Update()
    {
        playerParameters = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerParameters>();
        showUI = GameObject.FindGameObjectWithTag("ColliderUI").GetComponent<ShowUI>();
    }
    public void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player" && Input.GetKeyUp(KeyCode.E))
        {
            playerParameters.SaveGame();
            //Debug.Log("Partie Sauvegardée");
            showUI.SaveSuccess();
        }
    }
}
