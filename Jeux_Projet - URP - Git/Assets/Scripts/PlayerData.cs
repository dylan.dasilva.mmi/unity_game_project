﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int level;
    public int doorUnlockedNumber;
    public float[] position;

    public PlayerData(PlayerParameters player)
    {
        //Debug.Log(player.doorUnlockedNumber);
        level = SceneManager.GetActiveScene().buildIndex;
        doorUnlockedNumber = player.doorUnlockedNumber;
        position = new float[3];
        position[0] = player.gameObject.transform.position.x;
        position[1] = player.gameObject.transform.position.y;
        position[2] = player.gameObject.transform.position.z;
    }
}
